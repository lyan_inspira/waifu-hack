var gulp = require('gulp');
var pug = require('gulp-pug');
var browserSync = require('browser-sync').create();

// Tasks
gulp.task('default', ['pug']);

gulp.task('pug', function () {
    return gulp.src(['views/*.pug', '!views/error.pug'])
        .pipe(pug({ pretty: true }))
        .pipe(gulp.dest('./docs/'));
});

// Watching
gulp.task('watch', function () {
    browserSync.init({
        port: 4000, //where is browser sync
        proxy: 'http://localhost:4000/', //what are we proxying?
        ui: { port: 4001 }, //where is the UI
        browser: ['safari'] //empty array of browsers
    });

    gulp.watch('views/*.pug', ['pug'])
        .on('change', browserSync.reload);

    gulp.watch('public/**/*.css', null)
        .on('change', browserSync.reload);
    gulp.watch('public/**/*.js', null)
        .on('change', browserSync.reload);

});