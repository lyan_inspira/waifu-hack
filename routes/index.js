var express = require('express');
var router = express.Router();
var multer = require('multer');
var mime = require('mime');
var zip = new require('node-zip')();
var _ = require('lodash');
var del = require('del');
var request = require('request');
var fs = require('file-system');
var Promise = require('bluebird');
var progress = require('request-progress');
var rimraf = require('rimraf');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '.' + mime.extension(file.mimetype))
  }
})


var upload = multer({ dest: 'public/uploads', storage: storage })

/* GET home page. */
router.get('/', function (req, res, next) {
  console.log(req.session.id);
  res.render('index', { title: 'Express', session: req.session.id });
});

router.post('/upload', upload.array('file[]', 32), (req, res, next) => {
  var file_name = Date.now();
  var download_path = 'public/downloads/' + file_name + '/';

  uploadToWaifu(req, file_name)

  let result = {
    files: req.files,
    id: file_name
  }

  res.json(result);
});

router.get('/download/:id', (req, res, next) => {
  var path = './zip/' + req.params.id + '.zip';
  res.download(path);
});

router.get('/download-output/:filename', (req, res, next) => {
  var path = './public/downloads/' + req.params.filename;
  res.download(path);
});

function uploadToWaifu(req, file_name) {
  var files = [];
  var url = 'http://waifu2x.udp.jp/api';

  var download_path = 'public/downloads/';


  var promises = [];
  req.files.reduce((promise, file) => {
    let formData = {
      file: {
        value: fs.readFileSync(file.path),
        options: {
          filename: file.filename,
          contentType: file.mimetype
        }
      },
      style: "photo",
      noise: "3",
      scale: "2",
    };
    let options = {
      url: url,
      method: 'POST',
      formData: formData
    }

    if (!fs.existsSync(download_path)) {
      fs.mkdirSync(download_path);
    }
    var stream = fs.createWriteStream(download_path + file.filename);

    return promise.then((result) => {
      return new Promise((resolve, reject) => {
        progress(request(options), {
          throttle: 0,
          delay: 0
        })
          .on('progress', (state) => {
            file.percent = state.percent;
            global.io.sockets.emit(req.session.id, JSON.stringify(req.files));
          })
          .pipe(stream);
        stream.on('finish', () => {
          resolve("done");
          console.log("finished : " + file.originalname);
        });
        stream.on('error', (err) => {
          console.log(err);
          reject(err);
        });
      })
    }).catch(console.error);

  }, Promise.resolve()).then(() => {

  });
}

module.exports = router;
