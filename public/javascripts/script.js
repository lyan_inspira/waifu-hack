$(document).ready(function () {
    var socket = io();
    var count = 0;

    var session = $('#session').val();

    socket.connect('/', {
        "transports": ['websocket']
    });

    var percent = 0;
    socket.on(session, function (data) {
        var data = JSON.parse(data);
        var total = 0;
        console.log(data);
        for (var i = 0; i < data.length; i++) {
            $('.progress').eq(i).html(data[i].percent * 100);
            total += (typeof data[i].percent !== 'undefined') ? data[i].percent : 0;

            if (data[i].percent == 1) {
                $('.file-name').eq(i).html("<a target='_blank' href='/download-output/" + data[i].filename + "'>" + data[i].filename + "</a>");
            }

        }
        console.log(total);
        if (total == 1 * count) {
            $('#spinner').addClass('hidden');
        }
    });


    function download() {
        $.ajax
    }
    $('#form-upload').submit(function (event) {
        $('#spinner').removeClass('hidden');
        event.stopImmediatePropagation();
        var formData = new FormData();
        var fileSelect = document.getElementById('file');
        var files = fileSelect.files;
        count = files.length;
        console.log("count: %s", count);
        for (var i = 0; i < files.length; i++) {
            var file = files[i];

            // Add the file to the request.
            formData.append('file[]', file, file.name);
        }

        var xhr = new XMLHttpRequest();

        xhr.open('POST', '/upload', true);

        xhr.send(formData);
        xhr.onreadystatechange = function () {

            $('.btn-upload').attr('disabled', 'disabled');
            if (this.readyState == 4 && this.status == 200) {
                var myArr = JSON.parse(this.responseText);
                $('#file-list').show();
                $('#download').attr('href', '/download/' + myArr.id);
                for (var i = 0; i < myArr.files.length; i++) {
                    var $fileItem = $('#file-item').clone();
                    $fileItem.find('.file-name').html(myArr.files[i].originalname);
                    $fileItem.removeAttr('id');
                    $fileItem.appendTo('#file-list');
                }
            }
        };

        return false;
    });
});